# Index

|  Index    |  Design Pattern                                   |
| ------    | -------                                           |
|     1     | [Factory Design Pattern](#factory-design-pattern) |
|     2     | [Builder Pattern](#builder-pattern)               |
|     3     | [Null Object Design Pattern](#null-object-design-pattern) |
|     4     | [Prototype Design Pattern](#prototype-design-pattern) |
|     5     | [Singleton Design Pattern](#singleton-design-pattern) |
|     6     | [References](#references)


# Factory Design Pattern

**Explanation :-** The factory pattern is a creational pattern that provides a template that can be used to create object of similar type.

In factory design pattern we can create different objects that have some similar characterstics.

# Image

![Factory Design Pattern](factory.png)

# Code

```javascript
class ShowRoom {
  constructor () {
    this.createCar = function (carName) {
      let car;
      if (carName === 'Audi')
      {
        car = new Audi();
      }
      else if (carName === 'Benz') 
      {
        car = new Benz();
      }
      return car;
    };
  }
}

class Audi {
  constructor () {
    this.newCar = 'Audi';
    this.message = function () {
      return `you choose the ${this.newCar} car`; 
    }
  }
}

class Benz {
  constructor () {
    this.newCar = 'Benz';
    this.message = function () {
      return `you choose the ${this.newCar} car`; 
    }
  }
}

const Car = new ShowRoom();

const audi = Car.createCar('Audi');
console.log(audi.message()); 
const benz = Car.createCar('Benz');
console.log(benz.message());  
```

Output

```javascript
you choose the Audi car
you choose the Benz car
```

# Builder Pattern

**Explanation :-** Builder pattern says that construct a complex object from a simple objects using step-by-step approach.

It is mostly used when the object is not created in single step.

```javascript
class Address {
    constructor (zip, street) {
        this.zip = zip;
        this.street = street;
    }
}
class User {
    constructor (name, age, phone, address) {
        this.name = name;
        this.age = age;
        this.phone = phone;
        this.address = address;
    }
}
const user = new User ('Bob', undefined, undefined, new Address ('064', 'New Delhi'));
console.log (user);
```

Output
```javascript
User {name: 'Bob', age: undefined, phone: undefined, address: Address}
address: Address {zip: '064', street: 'New Delhi'}
age: undefined
name: "Bob"
phone: undefined
```

When you create an object, every time you remember in which order you passed the parameters. To avoid this, we are using the builder design pattern.

```javascript
class Address {
    constructor (zip, street) {
        this.zip = zip;
        this.street = street;
    }
}
class User {
    constructor (name) {
        this.name = name;
    }
}
class UserBuilder {
    constructor (name) {
        this.user = new User (name);
    }
    setAge (age) {
        this.user.age = age;
        return this;
    }
    setPhone (phone) {
        this.user.phone = phone;
        return this;
    }
    setAddress (address) {
        this.user.address = address;
        return this;
    }
    build () {
        return this.user;
    }
}
let user1 = new UserBuilder ('Bob').build();
console.log (user1);
```
Output
```javascript
User {name: 'Bob'}
name: "Bob"
```

# Null Object Design Pattern

**Explanation :-** The Null object pattern is a design pattern that handles null references. Null objects can arise in a program and it is usually handled using if-else. This is problematic since we need to remember to always put this if-else check every time. Instead of putting every time if-else, we can create a separate class for handling null references.

```javascript
class User {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
    hasAccess () {
        return this.name === 'Bob';
    }
}
const users = [new User (1, 'Tom'),new User (2, 'Bob')];
function getUser (id) {
    return users.find (user => user.id === id);
}
function printUser (id) {
    const user = getUser (id);
    //if-else creates an issue.
    let name = 'Guest';
    if (user != null && user.name != null) {
        name = user.name;
    }
    console.log ('Hello ' + name);
    //checking user is null or not we will do it in separate class.
    if (user != null && user.hasAccess != null && user.hasAccess()) {
        console.log ('You have access');
    } else {
        console.log ('You are not allowed here');
    }
}
printUser (1);
printUser (2);
printUser (3);
```
Output
```javascript
Hello Tom
You are not allowed here
Hello Bob
You have access
Hello Guest
You are not allowed here
```

Here we will solve the issue using creating a separate class.

```javascript
class User {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
    hasAccess () {
        return this.name === 'Bob';
    }
}
class NullUser {
    constructor() {
        this.id = -1;
        this.name = 'Guest';
    }
    hasAccess () {
        return false;
    }
}
const users = [new User (1, 'Tom'),new User (2, 'Bob')];
function getUser (id) {
    const user = users.find (user => user.id === id);
    if (user == null) {
        return new NullUser ();
    } else {
        return user;
    }
}
function printUser (id) {
    const user = getUser (id);
    console.log ("Hello" + user.name);
    if (user.hasAccess()) {
        console.log ('You have Access');
    } else {
        console.log ('You are not allowed');
    }
}
printUser (1);
printUser (2);
printUser (3);
```
Output
```javascript
Hello Tom
You are not allowed here
Hello Bob
You have access
Hello Guest
You are not allowed here
```

# Prototype Design Pattern

**Explanation :-** Prototype creational pattern is used to instantiate object with some default value using an existing object.

It Clones the object and provides the existing properties to the cloned object using prototypal inheritance.

In prototypal inheritance, a prototype object acts as a blueprint from which other objects inherit when the constructor instantiates them.

# Image

![Prototype Pattern](prototype.jpg)

# Code

```javascript
var Bike = {
    drive(){
        console.log("Started Driving")
        },
    brake(){
        console.log("Stopping the bike")
    },
    numofWheels : 2  
} 

const car1 = Object.create(Bike);
car1.drive();
car1.brake();
console.log(car1.numofWheels);

const car2 = Object.create(Bike)
car2.drive();
car2.brake();
console.log(car2.numofWheels);
```

Output
```javascript
Started Driving
Stopping the bike
2
Started Driving
Stopping the bike
2
```

# Singleton Design Pattern

**Explanation :-** The singleton pattern is mostly used in case where you want a single object to co-ordinate actions across a system.

Singleton Pattern is mainly used when object creation is expensive, lots of time taking.

# Image

![Singleton Design Pattern](singleton.png)

# Code

```javascript
let instance = null;
class Printer {
    constructor (pages) {
        this.display = function() {
            console.log ('You are connecter You want to print ' + pages + ' pages');
        }
    }
    static getInstance (noOfPages) {
        if (!instance) {
            instance = new Printer (noOfPages);
        }
        return instance;
    }
}
var obj1 = Printer.getInstance (2);
console.log (obj1);
obj1.display();
var obj2 = Printer.getInstance (2);
console.log (obj2);
obj2.display();
console.log (obj1 == obj2);
```
output
```javascript
You are connecter You want to print 2 pages
You are connecter You want to print 2 pages
true
```
# References

[Wikipedia](https://www.wikipedia.org)
[Educative.io](https://www.educative.io/collection/page/5429798910296064/5725579815944192/5546411429986304)
